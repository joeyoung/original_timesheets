package org.timesheets.service;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.timesheets.service.SqlScripts;

import static org.junit.Assert.assertTrue;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 4:04 PM
 */
@ContextConfiguration(locations = "/persistence-beans.xml")
public abstract class DomainAwareBase extends AbstractJUnit4SpringContextTests {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void deleteAllEntites() {

        jdbcTemplate.batchUpdate(SqlScripts.deleteScripts);
    }

}
