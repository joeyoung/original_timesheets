package org.timesheets.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.timesheets.domain.Employee;
import org.timesheets.domain.Manager;
import org.timesheets.domain.Task;
import org.timesheets.service.dao.EmployeeDao;
import org.timesheets.service.dao.ManagerDao;

import static org.junit.Assert.*;

/**
 * Created By: jyoung
 * Date: 10/25/13
 * Time: 9:46 AM
 */
@ContextConfiguration(locations = "/persistence-beans.xml")
public class TimesheetServiceTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private TimesheetService timesheetService;

    // resources for accessing data during the testing
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private ManagerDao managerDao;


    @Before
    public void insertData() {
        jdbcTemplate.batchUpdate(SqlScripts.createScripts);
    }

    @After
    public void cleanUp() {
        jdbcTemplate.batchUpdate(SqlScripts.deleteScripts);
    }

    @Test
    public void testBusiestTask() {
        Task task = timesheetService.busiestTask();
        assertTrue(1 == task.getId());
    }

    @Test
    public void testTasksForEmployees() {
        Employee steve = employeeDao.find(1L);
        Employee bill = employeeDao.find(2L);

        assertEquals(2, timesheetService.tasksForEmployee(steve).size());
        assertEquals(1, timesheetService.tasksForEmployee(bill).size());
    }

    @Test
    public void testTasksForManagers() {
        Manager eric = managerDao.find(1L);
        assertEquals(1, timesheetService.tasksForManager(eric).size());
    }
}
