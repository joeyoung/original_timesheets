package org.timesheets.service;

/**
 * Created By: jyoung
 * Date: 10/25/13
 * Time: 9:42 AM
 */
public final class SqlScripts {
    public static String[] deleteScripts = new String[]{
            "delete from task_employee",
            "delete from timesheet;",
            "delete from task;",
            "delete from employee;",
            "delete from manager;",
    };

    public static String[] createScripts = new String[]{
            "delete from task_employee;",
            "delete from timesheet;",
            "delete from task;",
            "delete from employee;",
            "delete from manager;",
            "insert into employee values(1, 'management', 'Steve Jobs');",
            "insert into employee values(2, 'management', 'Bill Gates');",
            "insert into employee values(3, 'engineering', 'Steve Wozniak');",
            "insert into employee values(4, 'engineering', 'Paul Allen');",
            "insert into manager values(1, 'Eric Schmidt');",
            "insert into manager values(2, 'Steve Ballmer');",
            "insert into task values(1, 0, 'task 1', 1);",
            "insert into task values(2, 0, 'task 2', 2);",
            "insert into task_employee values (1, 1);",
            "insert into task_employee values (1, 3);",
            "insert into task_employee values (1, 4);",
            "insert into task_employee values (2, 2);",
            "insert into task_employee values (2, 1);",
            "insert into timesheet values(1, 5, 1, 1);",
            "insert into timesheet values(2, 8, 2, 3);",
    };
}
