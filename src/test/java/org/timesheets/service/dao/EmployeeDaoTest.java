package org.timesheets.service.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.timesheets.domain.Employee;
import org.timesheets.domain.Manager;
import org.timesheets.domain.Task;
import org.timesheets.domain.Timesheet;
import org.timesheets.service.DomainAwareBase;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 4:21 PM
 */
@ContextConfiguration(locations = "/persistence-beans.xml")
public class EmployeeDaoTest extends DomainAwareBase {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private ManagerDao managerDao;

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private TimesheetDao timesheetDao;

    @Test
    public void testAdd(){
        int size = employeeDao.list().size();
        employeeDao.add(new Employee("test-employee", "hackers"));

        assertTrue(size < employeeDao.list().size());
    }

    @Test
    public void testUpdate() {
        assertEquals(0, employeeDao.list().size());
        List<Employee> employees = Arrays.asList(
                new Employee("test-1", "test"),
                new Employee("test-2", "test"),
                new Employee("test-3", "test"));

        for(Employee employee : employees) {
            employeeDao.add(employee);
        }

        List<Employee> found = employeeDao.list();
        assertEquals(3, found.size());

        for(Employee employee : found){
            assertTrue(employees.contains(employee));
        }
    }

    @Test
    public void testRemove() {
        Employee employee = new Employee("test", "test");
        employeeDao.add(employee);

        assertEquals(employee, employeeDao.find(employee.getId()));

        employeeDao.remove(employee);
        assertEquals(null, employeeDao.find(employee.getId()));
    }

    @Test
    public void testRemoveEmployee() {
        Manager manager = new Manager("task-manager");
        managerDao.add(manager);

        Employee employee = new Employee("task-employee", "test");
        employeeDao.add(employee);

        Task task = new Task("test-task", manager, employee);
        taskDao.add(task);

        Timesheet timesheet = new Timesheet(employee, task, 100);
        timesheetDao.add(timesheet);

        assertFalse(employeeDao.removeEmployee(employee));

        timesheetDao.remove(timesheet);
        taskDao.remove(task);

        assertTrue(employeeDao.removeEmployee(employee));
    }

}
