package org.timesheets.integration;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import static junit.framework.Assert.assertNotNull;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 11:23 AM
 */
@ContextConfiguration(locations = "/persistence-beans.xml")
public class HibernateConfigurationTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private SessionFactory sessionFactory;  // spring will instantiate and prepare the session factory

    @Test
    public void testConfigurationIsValid() {
        assertNotNull(sessionFactory);
    }
}
