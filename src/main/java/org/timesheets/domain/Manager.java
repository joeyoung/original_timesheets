package org.timesheets.domain;

import javax.persistence.*;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 7:35 AM
 */
@Entity
@Table(name = "manager")
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    private Manager() {}

    public Manager(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Manager [" +
                "id=" + id +
                ", name='" + name + '\'' +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Manager)) return false;

        Manager manager = (Manager) o;

        if (!id.equals(manager.id)) return false;
        if (!name.equals(manager.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
    int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
