package org.timesheets.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 7:44 AM
 */
@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "task_employee", joinColumns = {@JoinColumn(name = "task_id")}, inverseJoinColumns = {@JoinColumn(name = "employee_id")})
    private List<Employee> assignedEmployees = new ArrayList<Employee>();

    @OneToOne
    @JoinColumn(name = "manager_id")
    private Manager manager;

    private boolean completed;
    private String description;

    private Task() {}

    public Task(String description, Manager manager, Employee... employees) {
        this.description = description;
        this.manager = manager;
        assignedEmployees.addAll(Arrays.asList(employees));
        completed = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public void addEmployee(Employee employee){
        assignedEmployees.add(employee);
    }

    public void removeEmployee(Employee employee){
        assignedEmployees.remove(employee);
    }

    public void complete() {
        completed = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        if (completed != task.completed) return false;
        if (!description.equals(task.description)) return false;
        if (!id.equals(task.id)) return false;
        if (!manager.equals(task.manager)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + manager.hashCode();
        result = 31 * result + (completed ? 1 : 0);
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Task [" +
                "id=" + id +
                ", manager=" + manager +
                ", completed=" + completed +
                ", description='" + description + '\'' +
                ']';
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public List<Employee> getAssignedEmployees() {
        return assignedEmployees;
    }
}
