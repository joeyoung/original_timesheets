package org.timesheets.domain;

import javax.persistence.*;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 7:48 AM
 */
@Entity
@Table(name = "timesheet")
public class Timesheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee who;

    @OneToOne
    @JoinColumn(name = "task_id")
    private Task task;

    private Integer hours;

    private Timesheet() {}

    public Timesheet(Employee who, Task task, Integer hours) {
        this.who = who;
        this.task = task;
        this.hours = hours;
    }

    public Employee getWho() {
        return who;
    }

    public Task getTask() {
        return task;
    }

    public Integer getHours() {
        return hours;
    }

    /**
     * Manage can alter hours before closing a task
     * @param hours
     */
    public void alterHours(Integer hours){
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "Timesheet [who=" + who + ", task=" + task +", hours=" + hours + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Timesheet)) return false;

        Timesheet timesheet = (Timesheet) o;

        if (!hours.equals(timesheet.hours)) return false;
        if (!id.equals(timesheet.id)) return false;
        if (!task.equals(timesheet.task)) return false;
        if (!who.equals(timesheet.who)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + who.hashCode();
        result = 31 * result + task.hashCode();
        result = 31 * result + hours.hashCode();
        return result;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Long getId() {
        return id;
    }
}
