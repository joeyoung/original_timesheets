package org.timesheets.service;

import org.timesheets.domain.Employee;
import org.timesheets.domain.Manager;
import org.timesheets.domain.Task;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 8:05 AM
 */
public interface TimesheetService {

    /**
     * @return Finds the busiest task (with the most employees). Returns {@code null} when tasks are empty
     */
    Task busiestTask();

    /**
     * Finds all the tasks for an employee
     * @param employee
     * @return Tasks
     */
    List<Task> tasksForEmployee(Employee employee);

    /**
     * Finds all the tasks for a manager
     * @param manager
     * @return Tasks
     */
    List<Task> tasksForManager(Manager manager);
}
