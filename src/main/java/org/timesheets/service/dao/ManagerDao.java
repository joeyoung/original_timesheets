package org.timesheets.service.dao;

import org.timesheets.domain.Manager;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 2:14 PM
 */
public interface ManagerDao extends GenericDao<Manager, Long> {
    /**
     * Tries to remove manager from the system.
     * @param manager Manager to remove
     * @return {@code true} if manager is not assigned to any task.
     * Else {@code false}.
     */
    boolean removeManager(Manager manager);
}
