package org.timesheets.service.dao;

import org.timesheets.domain.Timesheet;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 2:17 PM
 */
public interface TimesheetDao extends GenericDao<Timesheet, Long> {
    // no additional logic atm
}
