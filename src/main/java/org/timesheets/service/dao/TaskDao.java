package org.timesheets.service.dao;

import org.timesheets.domain.Task;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 2:16 PM
 */
public interface TaskDao extends GenericDao<Task, Long> {
    /**
     * Tries to remove task from the system.
     * @param task Task to remove
     * @return {@code true} if there is no timesheet created on task.
     * Else {@code false}.
     */
    boolean removeTask(Task task);
}
