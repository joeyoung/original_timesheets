package org.timesheets.service.dao;

import org.timesheets.domain.Employee;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 2:12 PM
 */
public interface EmployeeDao extends GenericDao<Employee, Long> {
    /**
     * Tries to remove employee from the system.
     * @param employee Employee to remove
     * @return {@code true} if employee is not assigned to any task
     * or timesheet. Else {@code false}.
     */
    boolean removeEmployee(Employee employee);
}
