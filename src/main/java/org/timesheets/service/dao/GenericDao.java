package org.timesheets.service.dao;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 7:51 AM
 */
public interface GenericDao<E, K> {
    void add(E entity);
    void update(E entity);
    void remove(E entity);
    E find(K key);
    List<E> list();
}
