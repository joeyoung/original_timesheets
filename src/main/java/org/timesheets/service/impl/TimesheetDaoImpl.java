package org.timesheets.service.impl;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import org.timesheets.domain.Timesheet;
import org.timesheets.service.dao.TimesheetDao;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 3:53 PM
 */

@SuppressWarnings("unchecked")
@Repository("timesheetDao")
public class TimesheetDaoImpl extends HibernateDao<Timesheet, Long> implements TimesheetDao {
    @Override
    public List<Timesheet> list() {
        return currentSession().createCriteria(Timesheet.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }
}
