package org.timesheets.service.impl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.timesheets.domain.Task;
import org.timesheets.service.dao.TaskDao;

import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 3:51 PM
 */
@SuppressWarnings("unchecked")
@Repository("taskDao")
public class TaskDaoImpl extends HibernateDao<Task, Long> implements TaskDao {

    @Override
    public boolean removeTask(Task task) {
        Query taskQuery = currentSession()
                .createQuery("from Timesheet t where t.task.id = :id")
                .setParameter("id", task.getId());

        // task mustn't be assigned to no timesheet
        if (!taskQuery.list().isEmpty()) {
            return false;
        }

        // ok, remove as usual
        remove(task);
        return true;
    }

    @Override
    public List<Task> list() {
        return currentSession().createCriteria(Task.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
    }
}
