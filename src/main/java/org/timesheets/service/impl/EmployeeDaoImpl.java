package org.timesheets.service.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.timesheets.domain.Employee;
import org.timesheets.service.dao.EmployeeDao;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 3:02 PM
 */
@Repository("employeeDao")
public class EmployeeDaoImpl extends HibernateDao<Employee, Long> implements EmployeeDao {

    @Override
    public boolean removeEmployee(Employee employee) {
        Query taskQuery = currentSession()
                .createQuery("from Task t where :id in elements(t.assignedEmployees)")
                .setParameter("id", employee.getId());

        if (!taskQuery.list().isEmpty()) {
            return false;
        }

        Query timesheetQuery = currentSession()
                .createQuery("from Timesheet t where t.who.id = :id")
                .setParameter("id", employee.getId());

        if (!timesheetQuery.list().isEmpty()) {
            return false;
        }

        remove(employee);
        return true;
    }
}
