package org.timesheets.service.impl;

import org.timesheets.service.dao.GenericDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 7:54 AM
 */
public class InMemoryDao<E, K> implements GenericDao<E, K> {

    private List<E> entities = new ArrayList<E>();

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public void update(E entity) {
        // no implementation
    }

    @Override
    public void remove(E entity) {
        entities.remove(entity);
    }

    @Override
    public E find(K key) {
        if (entities.isEmpty()) {
            return  null;
        }

        // just return the first one since we are not using any keys at the moment
        return entities.get(0);
    }

    @Override
    public List<E> list() {
        return entities;
    }
}
