package org.timesheets.service.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.timesheets.domain.Manager;
import org.timesheets.service.dao.ManagerDao;

/**
 * Created By: jyoung
 * Date: 10/24/13
 * Time: 3:42 PM
 */
@Repository("managerDao")
public class ManagerDaoImpl extends HibernateDao<Manager, Long> implements ManagerDao {
    @Override
    public boolean removeManager(Manager manager) {
        Query managerQuery = currentSession()
                .createQuery("from Task t where t.manager.id = :id")
                .setParameter("id", manager.getId());

        // manager mustn't be assigned on no task
        if (!managerQuery.list().isEmpty()) {
            return false;
        }

        // ok, remove as usual
        remove(manager);
        return true;
    }
}
